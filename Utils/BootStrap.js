'use strict';

const DAOManager = require('../DAOManager').queries,
    Models = require('../Models'),
    Hash = require('./UniversalFunctions').CryptData,
    Config = require('../Config')



async function bootStrapAdmin() {
        let admins = {
                email           :   'admin@gmail.com',
                password        :   Hash('123456789'),
                timeZone        :   'Asia/Kolkata',
                isSuperAdmin    :   true,
                name            :   'Admin'
            }

        let promises = []

        let criteria = {
            email: admins.email
        };

        let setQuery = {
            $set: {
                email           : admins.email,
                password        : admins.password,
                timeZone        : admins.timeZone,
                isSuperAdmin    : admins.isSuperAdmin,
                name            : admins.name
            }
        };

        let options = {
            new: true,
            upsert: true
        };

        promises.push(DAOManager.findAndUpdate(Models.Client, criteria, setQuery, options));


        return await Promise.all(promises);
    }


module.exports = {
    bootStrapAdmin:bootStrapAdmin
};