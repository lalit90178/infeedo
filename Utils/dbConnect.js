const mongoose      =  require('mongoose'),
      dbConfig      =  require('../Config').dbConfig;

mongoose.Promise    =  global.Promise;

let dbUrl='';

    dbUrl="mongodb://"+dbConfig.DEFAULT.DB_IP+"/"+dbConfig.DEFAULT.DB_NAME;
    process.env.PORT=dbConfig.DEFAULT.PORT;





mongoose.connect(dbUrl,{ useMongoClient: true })
    .then(result =>{
        console.log('MongoDB Connected..........',process.env.NODE_ENV && process.env.NODE_ENV || 'DEFAULT');
    })
    .catch(reason =>{
        console.log("DB Error: ", reason);
        process.exit(1);
    });