const mongoose         =  require('mongoose'),
      Schema           =  mongoose.Schema,
      Config           =  require('../Config');


let User = new Schema({
    fullName               : {type:String, trim: true, index: true,required:true, sparse: true},
    email                  : {type:String, trim: true, index: true,unique:true},
    password               : {type:String,default:''},
    timeZone               : {type:String,default:''},
    createdDate            : {type:Date, default: new Date(),index:true,required: true},
    clientId               : {type: Schema.ObjectId, ref: "Client", default: null},
    accessToken            : {type:String, trim: true, index: true,sparse: true},
});


module.exports = mongoose.model('User', User);