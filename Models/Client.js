const mongoose         =  require('mongoose'),
      Schema           =  mongoose.Schema,
      Config           =  require('../Config');


let Client = new Schema({
    fullName               : {type:String, trim: true, index: true,required:true, sparse: true},
    email                  : {type:String, trim: true, index: true,unique:true},
    password               : {type:String,default:''},
    timeZone               : {type:String,default:''},
    createdDate            : {type:Date, default: new Date(),index:true,required: true},
    accessToken            : {type:String, trim: true, index: true,sparse: true},
    isSuperAdmin           : {type:Boolean, default:false}

});


module.exports = mongoose.model('Client', Client);