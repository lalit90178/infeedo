'use strict';

const DAOManager = require('../DAOManager').queries,
    Models = require('../Models'),
    UniversalFunctions = require('../Utils/UniversalFunctions'),
    TokenManager = require('../Lib/TokenManager'),
    Config = require('../Config');


async function login(payloadData) {
    try {
        let adminData;

        let criteria = {
            email: payloadData.email
        };

        let projection = {
            email: 1,
            name: 1,
            password: 1,
        };

        let options = {
            lean: true
        };
        adminData = await DAOManager.getData(Models.Client, criteria, projection, options);
        console.log("adminData*****************",adminData)
        if (!adminData || !adminData.length)
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);

        // ******* Checking Password

        if (!UniversalFunctions.compareHashPassword(payloadData.password, adminData[0].password))
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PASSWORD);

        // ****** GeneratingToken

        let tokenData = {
            id: adminData[0]._id,
            type: Config.APP_CONSTANTS.DATABASE.USER_ROLES.CLIENT
        };

        adminData[0].accessToken ="bearer " + (await  TokenManager.setToken(tokenData, Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN)).accessToken;

        delete adminData[0].password;
        delete adminData[0]._id;


        return adminData[0];
    } catch (err) {
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.SOMETHING_WENT_WRONG);
    }

}

async function addClient(payloadData) {
    try {

        let dataToSave = {
            fullName: payloadData.fullName,
            email: payloadData.email,
            password: UniversalFunctions.CryptData(payloadData.password),
            timeZone: payloadData.timeZone
        };


        return await DAOManager.saveData(Models.Client, dataToSave);


    } catch (err) {
        if (err.code == 11000)
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);
        else
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.SOMETHING_WENT_WRONG);
    }

}


async function addUser(payloadData) {
    try {

        let dataToSave = {
            fullName: payloadData.fullName,
            email: payloadData.email,
            clientId: payloadData.userId,                   // objectId of client
            password: UniversalFunctions.CryptData(payloadData.password),
            timeZone: payloadData.timeZone
        };


        return await DAOManager.saveData(Models.User, dataToSave);


    } catch (err) {
        if (err.code == 11000)
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);
        else
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.SOMETHING_WENT_WRONG);
    }

}


module.exports = {
    login: login,
    addClient: addClient,
    addUser: addUser,
};