
'use strict';

let SERVER = {
    APP_NAME: 'infeedo',
    PORTS: {
        HAPI:{
            DEV  : 8000
        }
    },
    TOKEN_EXPIRATION_IN_DAYS: '1d',
    JWT_SECRET_KEY: '1233',
};




let DATABASE = {
    PROJECT : 'infeedo',
    TOKEN_FIELDS :{
        ACCESS_TOKEN : 'accessToken'
    },
    USER_ROLES: {
        USER: 'USER',
        CLIENT: 'CLIENT',
    }

};

let STATUS_MSG = {
    ERROR: {
        SOMETHING_WENT_WRONG: {
            statusCode:400,
            type: 'SOMETHING_WENT_WRONG',
            customMessage : 'Something went wrong on server. '
        },
        INVALID_EMAIL: {
            statusCode:400,
            type: 'INVALID_EMAIL',
            customMessage : 'Email is not registered with us '
        },
        INVALID_PASSWORD: {
            statusCode:400,
            type: 'INVALID_PASSWORD',
            customMessage : 'Password does not match   '
        },
        EMAIL_ALREADY_EXIST: {
            statusCode:400,
            type: 'EMAIL_ALREADY_EXIST',
            customMessage : 'This email is already exist with another client '
        },
        ALREADY_EXIST: {
            statusCode:401,
            type: 'ALREADY_EXIST',
            customMessage : 'Already Exist '
        },
        INVALID_TOKEN: {
            statusCode:401,
            type: 'INVALID_TOKEN',
            customMessage : 'Invalid Token '
        },
        USER_NOT_RESGISTER:{
            statusCode:400,
            customMessage : 'User Is Not Register With Us',
            type : 'USER_NOT_RESGISTER'
        },
        IMP_ERROR:{
            statusCode:400,
            customMessage : 'Imp Error',
            type : 'IMP_ERROR'
        }

    },
    SUCCESS: {
        CREATED: {
            statusCode:201,
            customMessage : 'Created Successfully',
            type : 'CREATED'
        },
        DEFAULT: {
            statusCode:200,
            customMessage : 'Success',
            type : 'DEFAULT'
        },
        UPDATED: {
            statusCode:200,
            customMessage : 'Updated Successfully',
            type : 'UPDATED'
        },
        LOGIN:{
            statusCode:200,
            customMessage : 'Log In Successfully',
            type : 'LOGIN'
        },
        LOGOUT: {
            statusCode:200,
            customMessage : 'Logged Out Successfully',
            type : 'LOGOUT'
        },
        DELETED: {
            statusCode:200,
            customMessage : 'Deleted Successfully',
            type : 'DELETED'
        }
    }
};

let swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];


let APP_CONSTANTS = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    STATUS_MSG: STATUS_MSG,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages,
};

module.exports = APP_CONSTANTS;
