'use strict';


const AppConstants=require('./appConstants');

module.exports = {
    DEFAULT:{
        USER:'',
        PASS:'',
        DB_IP:'localhost',
        DB_NAME:'DEFAULT_DB_'+AppConstants.SERVER.APP_NAME,
        PORT:AppConstants.SERVER.PORTS.HAPI.DEV
    }
};