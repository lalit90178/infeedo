'use strict';

const Config    =   require('../Config'),
    AmazonSES   =   require('amazon-ses');
    let nodemailer = require('nodemailer');
    let sesTransport = require('nodemailer-ses-transport');

const sendEmail =async function(email, subject, content) {
    console.log("email***************",email)



    let transporter = nodemailer.createTransport(sesTransport({
        accessKeyId:Config.awsS3Config.s3Credentials.accessKeyId,
        secretAccessKey:Config.awsS3Config.s3Credentials.secretAccessKey,
        region:'us-west-2'
    }));

    transporter.sendMail({
        from: "Inffedo lalitkumar7404@gmail.com", // sender address
        to: email, // list of receivers
        subject: subject, // Subject line
        html: content
    }, function (err, data, res) {
        console.log('sendMail___________________---',err,data,res)
        return 1
    });
};


module.exports = {
    sendEmail:sendEmail
};