'use strict';

const Config          =  require('../Config'),
      Jwt             =  require('jsonwebtoken'),
      DOAManager      =  require('../DAOManager').queries,
      Models          =  require('../Models');


const getTokenFromDB = async function (userId, userType,token, userTokenType,field) {
    let criteria = {
        _id: userId
    };

    criteria[field] = token ;

    let data;

    if (userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER && userType == userTokenType)
        data = await DOAManager.getData(Models.User,criteria,{},{lean:true})
    else if(userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.CLIENT && userType == userTokenType)
        data = await DOAManager.getData(Models.Client,criteria,{},{lean:true});


    if(data && data.length){
        data[0].userType=userType;
        return Promise.resolve(data[0]);
    }else{
        if(field == Config.APP_CONSTANTS.DATABASE.TOKEN_FIELDS.ACCESS_TOKEN)
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        else 
            return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    }
};

const setTokenInDB = async function (userId,userType, tokenToSave,field) {
    let criteria = {
        _id: userId
    };
    let setQuery = {};

    setQuery[field] = tokenToSave;

    let data;

    console.log("const setTokenInDB = async function (userId,userType, tokenToSave,field) {");
         if(userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER)
        data = await DOAManager.findAndUpdate(Models.User,criteria,setQuery,{new:true});
    else if(userType === Config.APP_CONSTANTS.DATABASE.USER_ROLES.CLIENT)
        data = await DOAManager.findAndUpdate(Models.Client,criteria,setQuery,{new:true});

    if(data && Object.keys(data).length)
        return Promise.resolve(true);
    else
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
};

const expireTokenInDB = async function (userId,userType) {
    let criteria = {
        _id: userId
    };
    let setQuery = {
        $unset:{
            accessToken : 1,
            online : false
        }
    };

    let data;

    if(userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.USER)
        data = await DOAManager.findAndUpdate(Models.User,criteria,setQuery,{new:true});
    else if(userType == Config.APP_CONSTANTS.DATABASE.USER_ROLES.CLIENT)
        data = await DOAManager.findAndUpdate(Models.Client,criteria,setQuery,{new:true});

    if(data && Object.keys(data).length)
        return true;
    else
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
};

const verifyToken = async function (token,uerType,field) {
    let decoded=await Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
    if(decoded.id && decoded.type)
        return await getTokenFromDB(decoded.id, decoded.type,token, uerType || decoded.type,field);
    else
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
};

const setToken = async function (tokenData,field) {
    if (!tokenData.id || !tokenData.type) {
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        let tokenToSend = await Jwt.sign(tokenData, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
        await setTokenInDB(tokenData.id,tokenData.type, tokenToSend,field);
        return Promise.resolve({accessToken: tokenToSend});
    }

};

const expireToken = async function (token) {
    let decoded= await Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
    if(decoded.id && decoded.type)
        return await expireTokenInDB(decoded.id, decoded.type);
    else
        return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
};

const decodeToken = function (token) {
    return Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
};

const generateToken = function(tokenData){
    return  Jwt.sign(tokenData, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
}

module.exports = {
    expireTokenInDB:expireTokenInDB,
    expireToken: expireToken,
    setToken: setToken,
    verifyToken: verifyToken,
    decodeToken: decodeToken,
    generateToken : generateToken
};