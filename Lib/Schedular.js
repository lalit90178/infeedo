let schedule             = require('node-schedule'),
    DAOManager           = require('../DAOManager').queries,
    Models               = require('../Models'),
    NotificationManager  =  require('../Lib/NotificationManager')


exports.schedular = function () {

    let schedulerJob = schedule.scheduleJob('*/30 * * * *', function () {    // will get execuate in every 1/2 hour

        async function getUserData() {
            let userData;
            let email=[]
            let aggregateArray = [
                {
                    $group:{                                    // here groupby timezones and push email to array
                        _id:'$timeZone',
                        email:{
                            $push:'$email'
                        }
                    }
                },
                {
                    $project: {
                        hour:{ $hour: { date:new Date(), timezone: "$_id"} },      //calculate hour and minute for every timezone
                        minute:{ $minute: { date:new Date(), timezone: "$_id"} },
                        email:1
                    }
                },
                {
                    $match:{                                // match time is 8:00 am
                        "hour":8,
                        "minute":0
                    }
                }
            ];
            userData = await DAOManager.aggregateData(Models.User, aggregateArray,{});
            if(userData && userData.length >0){
                email=userData[0].email
                let subject = 'Greeting From Infeedo'
                let content = 'Good Morning from infeedo'
                NotificationManager.sendEmail(email,subject,content)            // send email to user
            }

        }

        getUserData()
    })
}




