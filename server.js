
'use strict';

const   Hapi               =  require('hapi'),
        Server             =  new Hapi.Server(),
        Routes             =  require('./Routes'),
        dbConnect          =  require('./Utils/dbConnect'),
        Bootstrap          =  require('./Utils/BootStrap'),
        Plugins            =  require('./Plugins'),
        Schedular          = require('./Lib/Schedular');



Server.connection({
    port:process.env.PORT || 8000,
    routes: { cors: true }
});

Server.register(Plugins, function(err){
    if(err){
        throw err;
    }
    Server.route(Routes);
});


Server.route(
    [
        {
            method: 'GET',
            path: '/',
            handler: function (req, res) {
                //TODO Change for production server
                res('Hello Thanks for visiting here')
            }
        }
    ]);


// Bootstrap admins................

Bootstrap.bootStrapAdmin()
    .then(data =>{
        console.log("Bootstrap for Admin is successfull......",data);
    })
    .catch(reason =>{
        console.log("Error occured in bootstrap admin......",reason);
    });

Schedular.schedular()                   // call schedular for execute every 30 minute to get the users

Server.start(
    console.log("server is running"),
    console.log('Server running at:', Server.info.uri)
);


