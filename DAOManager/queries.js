'use strict';

let saveData = function(model,data){
    return new model(data).save();
};

let getData = function (model, query, projection, options) {
    return model.find(query, projection, options);
};


let findAndUpdate = function (model, conditions, update, options) {
    return  model.findOneAndUpdate(conditions, update, options);
};

let aggregateData = function (model, aggregateArray,options) {

    let aggregation = model.aggregate(aggregateArray);

    if(options)
        aggregation.options = options;

    return aggregation.exec();
};

let insert = function(model, data, options){
    return model.collection.insert(data,options);
};




module.exports = {
    saveData : saveData,
    getData: getData,
    insert : insert,
    findAndUpdate : findAndUpdate,
    aggregateData : aggregateData,
};