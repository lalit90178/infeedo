const Controller = require('../Controllers'),
    UniversalFunctions = require('../Utils/UniversalFunctions'),
    Joi = require('joi'),
    Config = require('../Config');

let AuthRoutes = [

    {
        method: 'POST',
        path: '/admin/addClient',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            if (userData && userData._id && userData.userType) {
                request.payload.userId = userData._id;
            }
            if (userData && userData.isSuperAdmin == true) {
                Controller.AdminController.addClient(request.payload)
                    .then(result => {
                        reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, result));
                    })
                    .catch(reason => {
                        reply(UniversalFunctions.sendError(reason));
                    })
            } else {
                var err = "You are authorized to create client";
                reply(UniversalFunctions.sendError(err));
            }
        },
        config: {
            description: 'add Client API...',
            auth: 'ClientAuth',
            tags: ['api', 'admin'],
            validate: {
                payload: {
                    fullName: Joi.string().trim().required(),
                    email: Joi.string().email().required(),
                    password: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/),
                    timeZone: Joi.string().required().description('Send like "Asia/Kolkata" for timezone')
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/admin/addUser',
        handler: function (request, reply) {
            let userData = request.auth && request.auth.credentials && request.auth.credentials.userData;

            if (userData && userData._id && userData.userType) {
                request.payload.userId = userData._id;
            }
            Controller.AdminController.addUser(request.payload)
                .then(result => {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, result));
                })
                .catch(reason => {
                    reply(UniversalFunctions.sendError(reason));
                })

        },
        config: {
            description: 'add User API...',
            auth: 'ClientAuth',
            tags: ['api', 'admin'],
            validate: {
                payload: {
                    fullName: Joi.string().trim().required(),
                    email: Joi.string().email().required(),
                    password: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/),
                    timeZone: Joi.string().required().description('Send like "Asia/Kolkata" for timezone')
                },
                headers: UniversalFunctions.authorizationHeaderObj,
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

];

let NonAuthRoutes = [
    {
        method: 'POST',
        path: '/admin/adminLogin',
        handler: function (request, reply) {
            Controller.AdminController.login(request.payload)
                .then(result => {
                    reply(UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.LOGIN, result));
                })
                .catch(reason => {
                    reply(UniversalFunctions.sendError(reason));
                })
        },
        config: {
            description: 'Admin Login...',
            tags: ['api', 'admin'],
            validate: {
                payload: {
                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];

module.exports = [...AuthRoutes, ...NonAuthRoutes];